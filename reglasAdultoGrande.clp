(defrule aag1
  (edad adulto)
  (tamano grande)
  (pelaje largo)
  (estado saludable)
  (alimentacion industrial)
  (preferencia economico)
  =>
  (printout t "Te recomendamos Combinación Pienso Comercial + Pienso Premium Tamaño Grande" crlf)
)

(defrule aag2
  (edad adulto)
  (tamano grande)
  (pelaje largo)
  (estado saludable)
  (alimentacion industrial)
  (preferencia medio)
  =>
  (printout t "Te recomendamos Pienso Adulto Premium Tamaño Grande, Combinación Pienso Premium + Pienso Super Premium Tamaño Grande" crlf)
)

(defrule aag3
  (edad adulto)
  (tamano grande)
  (pelaje largo)
  (estado saludable)
  (alimentacion industrial)
  (preferencia noEconomico)
  =>
  (printout t "Pienso Adulto Super Premium Tamaño Grande)" crlf)
)

(defrule aag4
  (edad adulto)
  (tamano grande)
  (pelaje largo)
  (estado saludable)
  (alimentacion elaborada)
  (preferencia economico)
  =>
  (printout t "Te recomendamos Combinación Pienso Premium Tamaño Grande + Comida Preparada" crlf)
)

(defrule aag5
  (edad adulto)
  (tamano grande)
  (pelaje largo)
  (estado saludable)
  (alimentacion elaborada)
  (preferencia medio)
  =>
  (printout t "Te recomendamos Combinación Pienso Premium Tamaño Grande + Comida Preparada " crlf)
)

(defrule aag6
  (edad adulto)
  (tamano grande)
  (pelaje largo)
  (estado saludable)
  (alimentacion elaborada)
  (preferencia noEconomico)
  =>
  (printout t "Te recomendamos Alimento húmedo industrial, Combinacion Pienso Super Premium Tamaño Grande  + Comida Preparada" crlf)
)

(defrule aag7
  (edad adulto)
  (tamano grande)
  (pelaje largo)
  (estado enfermo)
  (alimentacion industrial)
  (preferencia medio)
  =>
  (printout t "Combinación Pienso Premium + Pienso Super Premium Tamaño Grande" crlf)
)

(defrule aag8
  (edad adulto)
  (tamano grande)
  (pelaje largo)
  (estado enfermo)
  (alimentacion industrial)
  (preferencia noEconomico)
  =>
  (printout t "Te recomendamos Pienso Adulto Super Premium Tamaño Grande" crlf)
)
(defrule aag9
  (edad adulto)
  (tamano grande)
  (pelaje largo)
  (estado enfermo)
  (alimentacion elaborada)
  (or (preferencia medio) (preferencia economico))
  =>
  (printout t "Te recomendamos Combinación Pienso Super Premium Tamaño Grande  + Comida Preparada " crlf)
)
(defrule aag10
  (edad adulto)
  (tamano grande)
  (pelaje largo)
  (estado enfermo)
  (alimentacion elaborada)
  (preferencia noEconomico)
  =>
  (printout t "Te recomendamos Alimento húmedo industrial, Combinacion Pienso Super Premium Tamaño Grande  + Comida Preparada" crlf)
)



(defrule aag11
  (edad adulto)
  (tamano grande)
  (pelaje corto)
  (estado saludable)
  (alimentacion industrial)
  (preferencia economico)
  =>
  (printout t "Te recomendamos Pienso Adulto Comercial Tamaño Grande, Combinación Pienso Comercial + Pienso Premium Tamaño Grande" crlf)
)
(defrule aag12
  (edad adulto)
  (tamano grande)
  (pelaje corto)
  (estado saludable)
  (alimentacion industrial)
  (preferencia medio)
  =>
  (printout t "Te recomendamos Pienso Adulto Premium Tamaño Grande, Pienso Premium + Pienso Super Premium Tamaño Grande " crlf)
)

(defrule aag13
  (edad adulto)
  (tamano grande)
  (pelaje corto)
  (estado saludable)
  (alimentacion industrial)
  (preferencia noEconomico)
  =>
  (printout t "Te recomendamos Pienso Adulto Super Premium Tamaño Grande " crlf)
)
(defrule aag14
  (edad adulto)
  (tamano grande)
  (pelaje corto)
  (estado saludable)
  (alimentacion elaborada)
  (preferencia economico)
  =>
  (printout t "Te recomendamos Combinación Pienso Premium Tamaño Grande + Comida Preparada" crlf)
)
(defrule aag15
  (edad adulto)
  (tamano grande)
  (pelaje corto)
  (estado saludable)
  (alimentacion elaborada)
  (preferencia medio)
  =>
  (printout t "Te recomendamos Combinación Pienso Premium Tamaño Grande + Comida Preparada " crlf)
)
(defrule aag16
  (edad adulto)
  (tamano grande)
  (pelaje corto)
  (estado saludable)
  (alimentacion elaborada)
  (preferencia noEconomico)
  =>
  (printout t "Te recomendamos Alimento húmedo industrial, Combinación Pienso SuperPremium Tamaño Grande + Comida Preparada " crlf)
)
(defrule aag17
  (edad adulto)
  (tamano grande)
  (pelaje corto)
  (estado enfermo)
  (alimentacion industrial)
  (or (preferencia medio) (preferencia economico))
  =>
  (printout t "Te recomendamos Combinación Pienso Premium + Pienso Super Premium Tamaño Grande" crlf)
)
(defrule aag18
  (edad adulto)
  (tamano grande)
  (pelaje corto)
  (estado enfermo)
  (alimentacion industrial)
  (preferencia noEconomico)
  =>
  (printout t "Te recomendamos Pienso Adulto Super Premium Tamaño Grande" crlf)
)
(defrule aag19
  (edad adulto)
  (tamano grande)
  (pelaje corto)
  (estado enfermo)
  (alimentacion elaborada)
  (or (preferencia medio) (preferencia economico))
  =>
  (printout t "Te recomendamos Pienso SuperPremium Tamaño Grande + Comida Preparada" crlf)
)
(defrule aag20
  (edad adulto)
  (tamano grande)
  (pelaje corto)
  (estado enfermo)
  (alimentacion elaborada)
  (preferencia noEconomico)
  =>
  (printout t "Te recomendamos Alimento húmedo industrial, Combinación Pienso SuperPremium Tamaño Grande + Comida Preparada " crlf)
)