;;*********************************************************************************
;;**************************  *****************************
;;*********************************************************************************

;;********************* DEFINICION de FUNCIONES ***********************************
;;************* Funciones que permiten capturar por teclado ***********************
;;*********************************************************************************

(deffunction preguntas (?pregunta $?valor)
   (printout t ?pregunta)
   (bind ?respuesta (read))
   (if (lexemep ?respuesta)
        then (bind ?respuesta (lowcase ?respuesta)))
   (while (not (member ?respuesta ?valor)) do
      (printout t ?pregunta)
      (bind ?respuesta (read))
      (if (lexemep ?respuesta)
           then (bind ?respuesta (lowcase ?respuesta))))
   ?respuesta)  

(deffunction si-o-no-p (?pregunta)
   (bind ?responde (preguntas ?pregunta si no s n))
   (if (or (eq ?responde si) (eq ?responde s))
       then TRUE
        else FALSE))

(deffunction menu ($?opciones)
   (printout t crlf)
   (printout t "************************ PetChef  *******************************" crlf)
   (printout t "PetChef coloca a disposición sus servicios con el fin de" crlf)
   (printout t "ayudar en la toma de decisión al momento de elegir  " crlf)
   (printout t "la alimentación adecuada para su mascota canina." crlf)
   (printout t crlf)
   (printout t "WARNING. PetChef no debe usarse como un reemplazo de un veterinario." crlf)
   (printout t "Lleve su canino a un veterinario si necesita asesoría profesional." crlf)
   (printout t crlf)
   (printout t "*************************** BIENVENIDO ****************************" crlf)
   (printout t "Seleccione la edad de su mascota:" crlf)
   (printout t "1. Cachorro" crlf)
   (printout t "2. Adulto" crlf)
   (printout t "3. Senior" crlf)
   (printout t "4. Salir" crlf)
   (printout t "Elija una opción: ")
   (bind ?opcion (read))
   (while (not (member ?opcion ?opciones)) do
      (printout t "Elija una opción: ")
      (bind ?opcion (read))
  )
   (assert (menuop ?opcion))
   (assert (fin 0))
   (printout t "" crlf)
   ?opcion
)

(deffunction menuRaza ($?opciones)
   (printout t crlf)
   (printout t "Seleccione la raza de su mascota:" crlf)
   (printout t "1. Boxer" crlf)
   (printout t "2. Salchicha" crlf)
   (printout t "3. Caniche" crlf)
   (printout t "4. Dogo" crlf)
   (printout t "5. Doberman" crlf)
   (printout t "6. Labrador" crlf)
   (printout t "7. Siberiano" crlf)
   (printout t "8. Ovejero Aleman" crlf)
   (printout t "9. Cocker" crlf)
   (printout t "10. Pitbull" crlf)
   (printout t "11. Golden" crlf)
   (printout t "12. Otro" crlf)
   (printout t "Elija una opción: ")
   (bind ?opcion (read))
   (while (not (member ?opcion ?opciones)) do
      (printout t "Elija una opción: ")
      (bind ?opcion (read))
  )
   (assert (menuopraza ?opcion))
   (assert (fin 0))
   (printout t "" crlf)
   ?opcion
)

(deffunction menuPreferencia ($?opciones)
   (printout t crlf)
   (printout t "Cúanto estás dispuesto a gastar en tu perro?" crlf)
   (printout t "1. Lo más económico" crlf)
   (printout t "2. Medio" crlf)
   (printout t "3. A mi no me importa el dinero" crlf)
   (printout t "Elija una opción: ")
   (bind ?opcion (read))
   (while (not (member ?opcion ?opciones)) do
      (printout t "Elija una opción: ")
      (bind ?opcion (read))
  )
   (assert (menuoppreferencia ?opcion))
   (assert (fin 0))
   (printout t "" crlf)
   ?opcion
)

(deffunction determinarAlimentacion()
    (if (si-o-no-p "Tu perro es alimentado con comida casera?: (SI/NO): ")
       then (assert (alimentacion elaborada))
            (menuPreferencia 1 2 3)
       else (assert(alimentacion industrial))
            (menuPreferencia 1 2 3)
   )
)

(deffunction determinarEstado()
    (if (si-o-no-p " Tu perro está saludable?: (SI/NO): ")
       then (assert (estado saludable))
            (determinarAlimentacion)
       else (assert(estado enfermo))
            (determinarAlimentacion)
   )
)

(defrule preferenciaEconomico
    (menuoppreferencia 1)
    =>
    (assert (preferencia economico))
)

(defrule preferenciaMedio
    (menuoppreferencia 2)
    =>
    (assert (preferencia medio))
)

(defrule preferenciaNoEconomico
    (menuoppreferencia 3)
    =>
    (assert (preferencia noEconomico))
)


;;*********************************************************************
;;********************** Reglas para raza ******************************
;;*********************************************************************


(defrule razaboxer
   (menuopraza 1)
   =>
  (assert (pelaje corto))
  (assert (tamano chico))
  (determinarEstado)
)

(defrule razasalchica
   (menuopraza 2)
   =>
  (assert (pelaje corto))
  (assert (tamano grande))
  (determinarEstado)
)

(defrule razacaniche
    (menuopraza 3)
    =>
    (assert (pelaje largo))
    (assert (tamano chico))
	(determinarEstado)
)

(defrule razaDogo
    (menuopraza 4)
    => 
    (assert (pelaje corto))
    (assert (tamano grande))
	(determinarEstado)
)
(defrule razaDoberman
    (menuopraza 5)
    =>
    (assert (pelaje corto))
    (assert (tamano grande))
	(determinarEstado)
) 
(defrule razaLabrador
    (menuopraza 6)
    =>
    (assert (pelaje largo))
    (assert (tamano grande))
	(determinarEstado)
)
(defrule razaSiberiano
    (menuopraza 7)
    =>
    (assert (pelaje largo))
    (assert (tamano grande))
	(determinarEstado)
)
(defrule razaOvejero
    (menuopraza 8)
    =>
    (assert (peleaje largo))
    (assert (tamano grande))
	(determinarEstado)
)    
(defrule razaCocker
    (menuopraza 9)
    =>
    (assert (pelaje largo))
    (assert (tamano chico))
	(determinarEstado)
)
(defrule razaPitbull
    (menuopraza 10)
    =>
    (assert (pelaje corto))
    (assert (tamano grande))    
	(determinarEstado)
)
(defrule razaGolden
    (menuopraza 11)
    => 
    (assert (pelaje largo))
    (assert (tamano grande))
	(determinarEstado)
)
(defrule razaOtro
    (menuopraza 12)
    => 
    (if (si-o-no-p " Su perro es de tamaño grande?: (SI/NO): ")
       then (assert (tamano grande))
            (if (si-o-no-p " Su perro es de pelaje largo?: (SI/NO): ")
                then (assert (pelaje largo))
                    (determinarEstado)
                else (assert (pelaje corto))
                    (determinarEstado)
            )
       else (assert (tamano chico))
            (if (si-o-no-p " Su perro es de pelaje largo?: (SI/NO): ")
                then (assert (pelaje largo))
                    (determinarEstado)
                else (assert (pelaje corto))
                    (determinarEstado)
            )
    )
)

;;*********************************************************************
;;********************** Reglas para Cachorro ******************************
;;*********************************************************************

(defrule ac1
   (menuop 1)
   =>
   (if (si-o-no-p " Su perro es mayor a 45 días?: (SI/NO): ")
       then (assert (edad cachorro))
            (menuRaza 1 2 3 4 5 6 7 8 9 10 11 12) 
       else (printout t "Te recomendamos continuar con la lactancia de tu cachorro hasta pasado los 45 días." crlf)
   )
)

(defrule ac2
   (edad cachorro)
   (pelaje largo)
   (estado saludable)
   (alimentacion industrial)
   (preferencia economico)
   =>
   (printout t "Te recomendamos combinar Pienso Cachorro comercial y Pienso Cachorro Premium" crlf)
)

(defrule ac3
   (edad cachorro)
   (pelaje largo)
   (estado saludable)
   (alimentacion industrial)
   (preferencia medio)
   =>
   (printout t "Te recomendamos alimentarlo con Pienso Cachorro Premium" crlf)
   (printout t "o bien, combinar Pienso Cachorro Premium y Pienso Cachorro Super Premium" crlf)
)

(defrule ac4
   (edad cachorro)
   (pelaje largo)
   (estado saludable)
   (alimentacion industrial)
   (preferencia noEconomico)
   =>
   (printout t "Te recomendamos alimentarlo con Pienso Cachorro Super Premium" crlf)
)

(defrule ac5
   (edad cachorro)
   (pelaje largo)
   (estado enfermo)
   (alimentacion industrial)
   (or(preferencia medio)(preferencia economico))
   =>
   (printout t "Te recomendamos Pienso Cachorro Premium " crlf)
   (printout t "o bien, combinar Pienso Cachorro  Premium y Pienso Cachorro Super Premium" crlf)
)

(defrule ac6
   (edad cachorro)
   (pelaje largo)
   (estado enfermo)
   (alimentacion industrial)
   (preferencia noEconomico)
   =>
   (printout t "Te recomendamos Pienso Cachorro Super Premium " crlf)
)

(defrule ac7
   (edad cachorro)
   (pelaje largo)
   (estado saludable)
   (alimentacion elaborada)
   (preferencia economico)
   =>
   (printout t "Te recomendamos combinar Pienso Cachorro Premium + Comida Preparada" crlf)
   (printout t "o bien, combinar Pienso Cachorro Comercial + Pienso Cachorro Premium" crlf)
)

(defrule ac8
   (edad cachorro)
   (pelaje largo)
   (estado saludable)
   (alimentacion elaborada)
   (preferencia medio)
   =>
   (printout t "Te recomendamos Pienso Cachorro Premium " crlf)
   (printout t "o bien, combinar Pienso Super Cachorro Premium + Comida Preparada" crlf)
   (printout t "y además, combinar Pienso Cachorro Premium + Pienso Cachorro Super Premium" crlf)
)

(defrule ac9
   (edad cachorro)
   (pelaje largo)
   (estado saludable)
   (alimentacion elaborada)
   (preferencia medio)
   =>
   (printout t "Te recomendamos Pienso Cachorro Super Premium " crlf)
   (printout t "o bien, Alimento húmedo industrial" crlf)
)

(defrule ac10
   (edad cachorro)
   (pelaje largo)
   (estado enfermo)
   (alimentacion elaborada)
   (or(preferencia medio)(preferencia economico))
   =>
   (printout t "Te recomendamos Pienso Cachorro Super Premium " crlf)
   (printout t "o bien,  Alimento humedo Industrial" crlf)
   (printout t "y además, combinar Pienso Cachorro Premium + Pienso Cachorro SuperPremium" crlf)
)

(defrule ac11
   (edad cachorro)
   (pelaje largo)
   (estado enfermo)
   (alimentacion elaborada)
   (preferencia noEconomico)
   =>
   (printout t "Te recomendamos Pienso Cachorro SuperPremium" crlf)
   (printout t "o bien, combinar  Pienso Premium + Pienso Super Premium" crlf)
)

(defrule ac12
   (edad cachorro)
   (pelaje corto)
   (estado saludable)
   (alimentacion industrial)
   (preferencia economico)
   =>
   (printout t "Te recomendamos combinar Pienso Cachorro comercial + Pienso Cachorro Premium" crlf)
)

(defrule ac13
   (edad cachorro)
   (pelaje corto)
   (estado saludable)
   (alimentacion industrial)
   (preferencia medio)
   =>
   (printout t "Te recomendamos Pienso Cachorro Premium" crlf)
   (printout t "o bien, combinar Pienso Cachorro Premium + Pienso Cachorro Super Premium" crlf)
)

(defrule ac14
   (edad cachorro)
   (pelaje corto)
   (estado saludable)
   (alimentacion industrial)
   (preferencia noEconomico)
   =>
   (printout t "Te recomendamos Pienso Cachorro Super Premium" crlf)
)

(defrule ac15
   (edad cachorro)
   (pelaje corto)
   (estado enfermo)
   (alimentacion industrial)
   (or(preferencia medio)(preferencia economico))
   =>
   (printout t "Te recomendamos Pienso Cachorro Premium" crlf)
   (printout t "o bien, combinar  Pienso Cachorro  Premium + Pienso Cachorro Super Premium" crlf)
)

(defrule ac16
   (edad cachorro)
   (pelaje corto)
   (estado enfermo)
   (alimentacion industrial)
   (preferencia noEconomico)
   =>
   (printout t "Te recomendamos Pienso Cachorro Super Premium" crlf)
)

(defrule ac17
   (edad cachorro)
   (pelaje corto)
   (estado saludable)
   (alimentacion elaborada)
   (preferencia economico)
   =>
   (printout t "Te recomendamos combinar Pienso Cachorro Premium + Comida Preparada" crlf)
   (printout t "o bien, combinar Pienso Cachorro Comercial + Pienso Cachorro Premium" crlf)
)

(defrule ac18
   (edad cachorro)
   (pelaje corto)
   (estado saludable)
   (alimentacion elaborada)
   (preferencia medio)
   =>
   (printout t "Te recomendamos Pienso Cachorro Premium," crlf)
   (printout t "o bien, combinar Pienso Super Cachorro Premium + Comida Preparada" crlf)
   (printout t "y además, combinar Pienso Cachorro Premium + Pienso Cachorro Super Premium" crlf)
)

(defrule ac19
   (edad cachorro)
   (pelaje corto)
   (estado saludable)
   (alimentacion elaborada)
   (preferencia noEconomico)
   =>
   (printout t "Te recomendamos Pienso Cachorro Super Premium" crlf)
   (printout t "o bien,  Alimento húmedo industrial" crlf)
)

(defrule ac20
   (edad cachorro)
   (pelaje corto)
   (estado enfermo)
   (alimentacion elaborada)
   (or(preferencia medio)(preferencia economico))
   =>
   (printout t "Te recomendamos Pienso Cachorro  Premium" crlf)
   (printout t "o bien,  Alimento húmedo Industrial" crlf)
   (printout t "y además, combinar  Pienso Cachorro Premium + Pienso Cachorro Super Premium" crlf)
)

(defrule ac21
   (edad cachorro)
   (pelaje corto)
   (estado enfermo)
   (alimentacion elaborada)
   (preferencia noEconomico)
   =>
   (printout t "Te recomendamos Pienso Cachorro Super Premium" crlf)
   (printout t "o bien, combinar Pienso Premium + Pienso Super Premium" crlf)
)

;;*********************************************************************
;;***************************** MENU **********************************
;;*********************************************************************

(defrule imprimir-menu
   (declare (salience 10))
   (not (menuop ?))
   =>
   (menu 1 2 3 4 5 6 7)
)

(defrule Salir
   (menuop 4)
   =>
   (exit)
)
