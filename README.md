# PeetChef

## Manual de Usuario de “PetChef”

### Introducción

El siguiente manual proporciona ayuda y características del sistema “PetCheff”, para que usted pueda interactuar de manera fácil y sencilla con este sistema. Fue elaborado íntegramente por los alumnos Débora A. Sudañez y Jorge J. Riera de la cátedra Ingeniería del Conocimiento de la Facultad de Ingeniería - Universidad de Jujuy.
El sistema fue realizado en CLIPS, porque permite manejar una amplia variedad de conocimiento, estos pueden ser representado como reglas heurísticas que especifican las acciones a ser ejecutadas, y soporta 3 tipos de programación: declarativo, empírico y orientado a objetos. 
Este manual pretende servir de base para la comprensión y correcta utilización del sistema experto desarrollado.

### Requerimientos del Sistema

- Debian 9 - Stretch
- GIT version 2.11.0
- CLIPS (V6.24 06/15/06)

### Instalación requerimientos
Para la instalación de los requerimientos, basta con correr el siguiente comando con privilegios de root.

`# sudo apt update`

`# sudo apt install git clips`

## Instalación “PetChef”
Para realizar la instalación del proyecto PetCheff, basta con clonar el repositorio del proyecto (https://gitlab.com/debit0/peetchef) con el siguiente comando.

`$ git clone https://gitlab.com/debit0/peetchef.git`

## Arrancando “PetChef”
El proyecto cuenta con la siguiente estructura:
```
peetchef
├── bootstrap
├── code.clp
├── README.md
├── reglasAdultoGrande.clp
├── reglasAdultoPequeno.clp
└── reglasViejo.clp
```
Para correr el proyecto, basta con lanzar el siguiente comando:

`$ clips -f bootstrap`

## Usando “PetChef”
PetChef  lanza un menú por consola, el cual permite seleccionar las opciones mediante introducción por teclado. Dependiendo de las respuestas, este le irá mostrando distintos menues y preguntas las cuales se responden mediante la introducción de caracteres “si” o “s” para una respuestas afirmativas, de otro modo “no” o “n” para respuestas negativas.
```
******************* PetChef API *******************************
PetChef coloca a disposición sus servicios con el fin de
ayudar en la toma de decisión al momento de elegir  
la alimentación adecuada para su mascota canina.

WARNING. PetChef no debe usarse como un reemplazo de un veterinario.
Lleve su canino a un veterinario si necesita asesoría profesional.

*************************** BIENVENIDO ****************************
Seleccione la edad de su mascota:
1. Cachorro
2. Adulto
3. Senior
4. Salir
Elija una opción:
```

Al finalizar el cuestionario, PetChef, le brinda las sugerencias correspondientes para la alimentación del canino. 
