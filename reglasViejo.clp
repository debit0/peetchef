(defrule av0
  (menuop 3)
  =>
   (assert (edad viejo))
   (menuRaza 1 2 3 4 5 6 7 8 9 10 11 12)
)

(defrule av1
  (edad viejo)
  (tamano pequeno)
  (estado saludable)
  (alimentacion industrial)
  (preferencia medio)
  =>
  (printout t "Te recomendamos Combinación Pienso Senior Premium Tamaño chico + Pienso Senior  SuperPremium - Tamaño chico " crlf)
)

(defrule av2
  (edad viejo)
  (tamano pequeno)
  (estado saludable)
  (alimentacion industrial)
  (preferencia noEconomico)
  =>
  (printout t "Te recomendamos Pienso Senior SuperPremium - Tamaño chico " crlf)
)

(defrule av3
  (edad viejo)
  (tamano pequeno)
  (estado saludable)
  (alimentacion elaborada)
  (preferencia medio)
  =>
  (printout t "Te recomendamos Alimento húmedo Industrial, Combinación de Pienso Senior SuperPremium Tamaño chico + Comida Preparada " crlf)
)

(defrule av4
  (edad viejo)
  (tamano pequeno)
  (estado saludable)
  (alimentacion elaborada)
  (preferencia noEconomico)
  =>
  (printout t "Te recomendamos Alimento húmedo Industrial, Combinación de Pienso Senior SuperPremium Tamaño chico + Comida Preparada  " crlf)
)

(defrule av5
  (edad viejo)
  (tamano pequeno)
  (estado enfermo)
  (alimentacion industrial)
  (or (preferencia medio) (preferencia economico))
  =>
  (printout t "Te recomendamos Pienso Senior SuperPremium Tamaño chico " crlf)
)
(defrule av6
  (edad viejo)
  (tamano pequeno)
  (estado enfermo)
  (alimentacion industrial)
  (preferencia noEconomico)
  =>
  (printout t "Te recomendamos Pienso Senior SuperPremium Tamaño chico  " crlf)
)
(defrule av7
  (edad viejo)
  (tamano pequeno)
  (estado enfermo)
  (alimentacion elaborada)
  (or (preferencia medio) (preferencia economico))
  =>
  (printout t "Te recomendamos combinación de Pienso Senior SuperPremium + Comida Preparada - Tamaño chico" crlf)
)
(defrule av8
  (edad viejo)
  (tamano pequeno)
  (estado enfermo)
  (alimentacion elaborada)
  (preferencia noEconomico)
  =>
  (printout t "Te recomendamos Combinación de Pienso Senior SuperPremium + Comida Preparada - Tamaño chico " crlf)
)

(defrule av9
  (edad viejo)
  (tamano grande)
  (estado saludable)
  (alimentacion industrial)
  (preferencia medio)
  =>
  (printout t "Te recomendamos Pienso Senior Premium + Pienso Senior  SuperPremium - Tamaño Grande " crlf)
)

(defrule av10
  (edad viejo)
  (tamano grande)
  (estado saludable)
  (alimentacion industrial)
  (preferencia noEconomico)
  =>
  (printout t "Te recomendamos Pienso Senior SuperPremium - Tamaño Grande " crlf)
)

(defrule av11
  (edad viejo)
  (tamano grande)
  (estado saludable)
  (alimentacion elaborada)
  (preferencia medio)
  =>
  (printout t "Te recomendamos Alimento húmedo Industrial, Combinación de Pienso Senior SuperPremium + Comida Preparada - Tamaño Grande " crlf)
)

(defrule av12
  (edad viejo)
  (tamano grande)
  (estado saludable)
  (alimentacion elaborada)
  (preferencia noEconomico)
  =>
  (printout t "Te recomendamos Alimento húmedo Industrial, Combinación de Pienso Senior SuperPremium + Comida Preparada - Tamaño Grande" crlf)
)

(defrule av13
  (edad viejo)
  (tamano grande)
  (estado enfermo)
  (alimentacion industrial)
  (or (preferencia medio) (preferencia economico))
  =>
  (printout t "Te recomendamos Pienso Senior SuperPremium - Tamaño Grande  " crlf)
)

(defrule av14
  (edad viejo)
  (tamano grande)
  (estado enfermo)
  (alimentacion industrial)
  (preferencia noEconomico)
  =>
  (printout t "Te recomendamos Pienso Senior SuperPremium - Tamaño Grande " crlf)
)
(defrule av15
  (edad viejo)
  (tamano grande)
  (estado enfermo)
  (alimentacion elaborada)
  (or (preferencia medio) (preferencia economico))
  =>
  (printout t "Te recomendamos Combinación de Pienso Senior SuperPremium + Comida Preparada - Tamaño Grande " crlf)
)
(defrule av16
  (edad viejo)
  (tamano grande)
  (estado enfermo)
  (alimentacion elaborada)
  (preferencia noEconomico)
  =>
  (printout t "Te recomendamos Combinación de Pienso Senior SuperPremium + Comida Preparada - Tamaño Grande " crlf)
)
