(defrule aap0
  (menuop 2)
  =>
   (assert (edad adulto))
   (menuRaza 1 2 3 4 5 6 7 8 9 10 11 12)
)

(defrule aap1
  (edad adulto)
  (tamano pequeno)
  (pelaje largo)
  (estado saludable)
  (alimentacion industrial)
  (preferencia economico)
  =>
  (printout t "Te recomendamos Pienso Comercial + Pienso Premium Tamaño chico" crlf)
)

(defrule aap2
  (edad adulto)
  (tamano pequeno)
  (pelaje largo)
  (estado saludable)
  (alimentacion industrial)
  (preferencia medio)
  =>
  (printout t "Te recomendamos Pienso Adulto Premium Tamaño Chico, Combinación Pienso Premium + Pienso Super Premium Tamaño chico" crlf)
)

(defrule aap3
  (edad adulto)
  (tamano pequeno)
  (pelaje largo)
  (estado saludable)
  (alimentacion industrial)
  (preferencia noEconomico)
  =>
  (printout t "Te recomendamos Pienso Adulto Super Premium Tamaño Chico" crlf)
)

(defrule aap4
  (edad adulto)
  (tamano pequeno)
  (pelaje largo)
  (estado saludable)
  (alimentacion elaborada)
  (preferencia economico)
  =>
  (printout t "Te recomendamos Combinación Pienso Premium Tamaño Chico + Comida Preparada" crlf)
)

(defrule aap5
  (edad adulto)
  (tamano pequeno)
  (pelaje largo)
  (estado saludable)
  (alimentacion elaborada)
  (preferencia medio)
  =>
  (printout t "Te recomendamos Combinación Pienso Premium Tamaño Chico + Comida Preparada " crlf)
)

(defrule aap6
  (edad adulto)
  (tamano pequeno)
  (pelaje largo)
  (estado saludable)
  (alimentacion elaborada)
  (preferencia noEconomico)
  =>
  (printout t "Te recomendamos Alimento húmedo industrial, Combinacion Pienso SuperPremium Tamaño Chico + Comida Preparada" crlf)
)

(defrule aap7
  (edad adulto)
  (tamano pequeno)
  (pelaje largo)
  (estado enfermo)
  (alimentacion industrial)
  (or (preferencia medio) (preferencia economico))
  =>
  (printout t "Te recomendamos Combinación Pienso Premium + Pienso Super Premium Tamaño chico" crlf)
)

(defrule aap8
  (edad adulto)
  (tamano pequeno)
  (pelaje largo)
  (estado enfermo)
  (alimentacion industrial)
  (preferencia noEconomico)
  =>
  (printout t "Te recomendamos Pienso Adulto Super Premium Tamaño Chico" crlf)
)
(defrule aap9
  (edad adulto)
  (tamano pequeno)
  (pelaje largo)
  (estado enfermo)
  (alimentacion elaborada)
  (or (preferencia medio) (preferencia economico))
  =>
  (printout t "Te recomendamos Combinación Pienso SuperPremium Tamaño Chico + Comida Preparada " crlf)
)
(defrule aap10
  (edad adulto)
  (tamano pequeno)
  (pelaje largo)
  (estado enfermo)
  (alimentacion elaborada)
  (preferencia noEconomico)
  =>
  (printout t "Te recomendamos Alimento húmedo industrial, Combinación Pienso SuperPremium Tamaño Chico + Comida Preparada" crlf)
)



(defrule aap11
  (edad adulto)
  (tamano pequeno)
  (pelaje corto)
  (estado saludable)
  (alimentacion industrial)
  (preferencia economico)
  =>
  (printout t "Te recomendamos Pienso Adulto Comercial Tamaño Chico, Combinación Pienso Comercial + Pienso Premium Tamaño chico" crlf)
)
(defrule aap12
  (edad adulto)
  (tamano pequeno)
  (pelaje corto)
  (estado saludable)
  (alimentacion industrial)
  (preferencia medio)
  =>
  (printout t "Te recomendamos Pienso Adulto Premium Tamaño Chico, Combinación Pienso Premium + Pienso Super Premium Tamaño chico " crlf)
)

(defrule aap13
  (edad adulto)
  (tamano pequeno)
  (pelaje corto)
  (estado saludable)
  (alimentacion industrial)
  (preferencia noEconomico)
  =>
  (printout t "Te recomendamos Pienso Adulto Super Premium Tamaño Chico " crlf)
)
(defrule aap14
  (edad adulto)
  (tamano pequeno)
  (pelaje corto)
  (estado saludable)
  (alimentacion elaborada)
  (preferencia economico)
  =>
  (printout t "Te recomendamos Combinación Pienso Premium Tamaño Chico + Comida Preparada" crlf)
)
(defrule aap15
  (edad adulto)
  (tamano pequeno)
  (pelaje corto)
  (estado saludable)
  (alimentacion elaborada)
  (preferencia medio)
  =>
  (printout t "Te recomendamos Combinación Pienso Premium Tamaño Chico + Comida Preparada " crlf)
)
(defrule aap16
  (edad adulto)
  (tamano pequeno)
  (pelaje corto)
  (estado saludable)
  (alimentacion elaborada)
  (preferencia noEconomico)
  =>
  (printout t "Te recomendamos Alimento húmedo industrial, Combinación Pienso SuperPremium Tamaño Chico + Comida Preparada " crlf)
)
(defrule aap17
  (edad adulto)
  (tamano pequeno)
  (pelaje corto)
  (estado enfermo)
  (alimentacion industrial)
  (or (preferencia medio) (preferencia economico))
  =>
  (printout t "Te recomendamos Combinación Pienso Premium + Pienso Super Premium Tamaño chico" crlf)
)
(defrule aap18
  (edad adulto)
  (tamano pequeno)
  (pelaje corto)
  (estado enfermo)
  (alimentacion industrial)
  (preferencia noEconomico)
  =>
  (printout t "Te recomendamos Pienso Adulto Super Premium Tamaño Chico" crlf)
)
(defrule aap19
  (edad adulto)
  (tamano pequeno)
  (pelaje corto)
  (estado enfermo)
  (alimentacion elaborada)
  (or (preferencia medio) (preferencia economico))
  =>
  (printout t "Te recomendamos Pienso SuperPremium Tamaño Chico + Comida Preparada" crlf)
)
(defrule aap20
  (edad adulto)
  (tamano pequeno)
  (pelaje corto)
  (estado enfermo)
  (alimentacion elaborada)
  (preferencia noEconomico)
  =>
  (printout t "Te recomendamos Alimento húmedo industrial, Combinación Pienso SuperPremium Tamaño Chico + Comida Preparada " crlf)
)

